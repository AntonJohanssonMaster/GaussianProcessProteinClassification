//This is the c++ code for the Gakco-string kernel
// Import it into r with the Rcpp package and call it as
//gakcokernel(sequences, gmersize, holes, training)
//Where sequences is a character vector
//gmersize,holes and training are numeric values

#include <Rcpp.h>
#include <iostream>
#include <vector>
#include <algorithm>

// Enable C++11 via this plugin (Rcpp 0.10.3 or later)
// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

//Creates the gmers from the full strings.
// [[Rcpp::export]]
std::vector< std::string > createGmer(std::vector< std::string > str, int g)
{


	int n0 = str[0].size();
	int n1 = str[1].size();
	std::vector<int> n{ n0, n1 };
	std::vector<std::string> vecOfStr((n[0] - g + 1) + (n[1] - g + 1), "");

	for (int k = 0; k < 2; ++k)
	{
		for (int i = 0; i < (n[k] - g + 1); ++i)
		{
			vecOfStr[i + k * ((n[0] - g + 1))] = str[k].substr(i, g);
		}
	}



	return vecOfStr;
}

//This vecto sorts the vector lexicollogically and the indices as well
// [[Rcpp::export]]
std::vector<int> sortera(std::vector< std::string > &x, std::vector<int> y)
{
	std::size_t n(0);
	std::generate(std::begin(y), std::end(y), [&] { return n++; });

	std::sort(std::begin(y),
		std::end(y),
		[&](int i1, int i2) { return x[i1] < x[i2]; });

	std::sort(x.begin(), x.end());

	return y;
}

//This function reorders the vector 1111,2222 with groups so that they correspond to the sorted one
// [[Rcpp::export]]
std::vector<int> reorder(std::vector<int> groupVec, std::vector<int> index)
{
	int n = index.size();
	std::vector<int> sortedGroup = groupVec;
	for (int i = 0; i < n; ++i)
	{
		sortedGroup[i] = groupVec[index[i]];
	}

	return sortedGroup;

}

//Finds the number of groups and calculates the C value
// [[Rcpp::export]]
int calcC(std::vector< std::string > gmerSequences, std::vector<int> groupIndex)
{
	int numberOfGmer = groupIndex.size();
	std::string loopString = "";
	int nGroup1 = 0;
	int nGroup2 = 0;
	int numGmerMatch = 0;
	//Loop to go through all groups
	for (int k = 0; k < numberOfGmer; ++k)
	{
		if (loopString != gmerSequences[k])
		{
			//If the strings were not equal and we have detected substrings from both groups
			if (k > 0 && nGroup1 > 0 && nGroup2 > 0)
			{
				numGmerMatch = numGmerMatch + nGroup1 * nGroup2;
			}
			//Set new substring into the old one
			loopString = gmerSequences[k];
			nGroup1 = 0;
			nGroup2 = 0;
			//Need the index for the first memeber of the group
			if (groupIndex[k] == 1)
			{
				nGroup1 += 1;
			}
			else
			{
				nGroup2 += 1;
			}
		}
		//Add groups members
		else
		{
			if (groupIndex[k] == 1)
			{
				nGroup1 += 1;
			}
			else
			{
				nGroup2 += 1;
			}
		}
		//Since the we check inequality we need a final check like this
		if (k == (numberOfGmer - 1) && nGroup1 > 0 && nGroup2 > 0)
		{
			numGmerMatch = numGmerMatch + nGroup1 * nGroup2;
		}
	}

	return numGmerMatch;
}
//Choose function
// [[Rcpp::export]]
unsigned nChoosek(unsigned n, unsigned k)
{
	if (k > n) return 0;
	if (k * 2 > n) /*return*/ k = n - k;  //remove the commented section
	if (k == 0) return 1;

	int result = n;
	for (int i = 2; i <= k; ++i) {
		result *= (n - i + 1);
		result /= i;
	}
	return result;
}

//N is length of sequence
//K is number of holes
// [[Rcpp::export]]
std::vector <int >  comb(int N, int K)
{
	std::vector <int > storeVector(K*nChoosek(N, K));
	std::string bitmask(K, 1); // K leading 1's
	bitmask.resize(N, 0); // N-K trailing 0's

	int k = 0;   // print integers and permute bitmask
	int pushBackIndex = 0;
	do {
		for (int i = 0; i < N; ++i) // [0..N-1] integers
		{
			if (bitmask[i])
			{
				storeVector[k] = i - pushBackIndex;
				k += 1;
				pushBackIndex += 1;
				//Have to pushback the indices due to the removal process
				if ((pushBackIndex % K) == 0)
				{
					pushBackIndex = 0;
				}
			} //std::cout << " " << i;
		}
	} while (std::prev_permutation(bitmask.begin(), bitmask.end()));
	return storeVector;

}
//This function removes elements from the substrings to insert the holes
// [[Rcpp::export]]
void insertHoles(std::vector< std::string > &protSeq, int holes, std::vector <int > &holeIndices)
{
	int numOfSeq = protSeq.size();
	int stepLength = 0;
	for (int iii = 0; iii < numOfSeq; ++iii)
	{
		for (int kkk = 0; kkk < holes; ++kkk)
		{
			protSeq[iii].erase(protSeq[iii].begin() + holeIndices[kkk]);
		}

	}


}
// [[Rcpp::export]]
NumericMatrix trainKernel(std::vector< std::string > &Allsequences, int gmerSize, int M) 
{
	std::vector< std::string > sequences(2); //Store the pair to be compared
	int rowSize = Allsequences.size(); //maybe there will be an issue here is we only revieve a pointer
	int colSize = Allsequences.size();

	NumericMatrix kernelMatrix(rowSize, colSize);

	for (int zzz = 0; zzz < rowSize; ++zzz)
	{
		std::vector <int > rows(colSize);

		for (int ccc = zzz; ccc < colSize; ++ccc)
		{
			sequences[0] = Allsequences[zzz];
			sequences[1] = Allsequences[ccc];

			int n0 = sequences[0].size();
			int n1 = sequences[1].size();



			//Denna kod fungerar i debugtime iallafall
			//Tror allting fungerar i runtime nu

			std::vector< std::vector <int > > holeIndexStore(M);
			for (int g = 1; g <= M; ++g)
			{
				holeIndexStore[g - 1] = comb(gmerSize, g);
			}
			sequences = createGmer(sequences, gmerSize);

			//for
			std::vector <int > Cstore(M + 1);
			//this forloop should loop and insert the holes and such

			std::vector<int> y(sequences.size());

			y = sortera(sequences, y);
			std::vector<int> vector1(n0 - gmerSize + 1, 1);
			std::vector<int> vector2(n1 - gmerSize + 1, 2);

			std::vector<int> concVec = vector1;
			concVec.insert(concVec.end(), vector2.begin(), vector2.end());
			std::vector<int> finalVecm0 = reorder(concVec, y);


			//Function here that checks how many groups we have
			//Send sequences and finalVec
			Cstore[0] = calcC(sequences, finalVecm0);




			for (int holeNum = M; holeNum <= M; ++holeNum)
			{


				//Loops through all the possible hole combinations

				//Store the c-values for the different holes
				for (int jjj = 0; jjj < holeIndexStore[holeNum - 1].size(); jjj = jjj + holeNum)
				{
					std::vector< std::string > sequenceStore = sequences;
					//Retrieves the hole positions
					std::vector <int > holePos(holeNum);
					for (int hhh = 0; hhh < holeNum; ++hhh)
					{
						holePos[hhh] = holeIndexStore[holeNum - 1][hhh + jjj];
					}
					//for some reason it does not sort accurately later...?

					insertHoles(sequenceStore, holeNum, holePos);

					std::vector<int> y(sequenceStore.size());

					y = sortera(sequenceStore, y);
					std::vector<int> vector1(n0 - gmerSize + 1, 1);
					std::vector<int> vector2(n1 - gmerSize + 1, 2);

					//std::vector<int> concVec = vector1;
					//concVec.insert(concVec.end(), vector2.begin(), vector2.end());
					std::vector<int> concVec = finalVecm0;
					std::vector<int> finalVec = reorder(concVec, y);


					//Function here that checks how many groups we have
					//Send sequences and finalVec
					Cstore[holeNum] = Cstore[holeNum] + calcC(sequenceStore, finalVec);
					//std::cout << Cstore[holeNum] << std::endl;
				}


			}

			std::vector <int > Nstore(M + 1);
			Nstore[0] = Cstore[0];

			for (int uuu = 1; uuu <= M; ++uuu)
			{
				int Cfactor = 0;
				for (int ttt = 0; ttt <= uuu; ++ttt)
				{
					Cfactor = Cfactor + nChoosek(gmerSize - ttt, M - ttt)*Nstore[ttt];

				}

				Nstore[uuu] = Cstore[uuu] - Cfactor;

			}

			int kernelValue = 0;

			for (int fff = 0; fff <= M; ++fff)
			{
				kernelValue = kernelValue + Nstore[fff] * nChoosek(gmerSize - fff, gmerSize - M);
			}

			kernelMatrix(zzz, ccc) = kernelValue;
			kernelMatrix(ccc, zzz) = kernelValue;
		}

	}

	return kernelMatrix;
	

}
// [[Rcpp::export]]
NumericMatrix predictKernel(std::vector< std::string > &Allsequences, int gmerSize, int M, int trainSize)
{
	std::vector< std::string > sequences(2); //Store the pair to be compared
	int rowSize = Allsequences.size();
	int colSize = Allsequences.size();

	NumericMatrix kernelMatrix(rowSize, trainSize);

	for (int zzz = 0; zzz < rowSize; ++zzz)
	{
		std::vector <int > rows(colSize);

		for (int ccc = colSize-trainSize; ccc < colSize; ++ccc)
		{
			sequences[0] = Allsequences[zzz];
			sequences[1] = Allsequences[ccc];

			int n0 = sequences[0].size();
			int n1 = sequences[1].size();



			//Denna kod fungerar i debugtime iallafall
			//Tror allting fungerar i runtime nu

			std::vector< std::vector <int > > holeIndexStore(M);
			for (int g = 1; g <= M; ++g)
			{
				holeIndexStore[g - 1] = comb(gmerSize, g);
			}
			sequences = createGmer(sequences, gmerSize);

			//for
			std::vector <int > Cstore(M + 1);
			//this forloop should loop and insert the holes and such

			std::vector<int> y(sequences.size());

			y = sortera(sequences, y);
			std::vector<int> vector1(n0 - gmerSize + 1, 1);
			std::vector<int> vector2(n1 - gmerSize + 1, 2);

			std::vector<int> concVec = vector1;
			concVec.insert(concVec.end(), vector2.begin(), vector2.end());
			std::vector<int> finalVecm0 = reorder(concVec, y);


			//Function here that checks how many groups we have
			//Send sequences and finalVec
			Cstore[0] = calcC(sequences, finalVecm0);




			for (int holeNum = M; holeNum <= M; ++holeNum)
			{


				//Loops through all the possible hole combinations

				//Store the c-values for the different holes
				for (int jjj = 0; jjj < holeIndexStore[holeNum - 1].size(); jjj = jjj + holeNum)
				{
					std::vector< std::string > sequenceStore = sequences;
					//Retrieves the hole positions
					std::vector <int > holePos(holeNum);
					for (int hhh = 0; hhh < holeNum; ++hhh)
					{
						holePos[hhh] = holeIndexStore[holeNum - 1][hhh + jjj];
					}
					//for some reason it does not sort accurately later...?

					insertHoles(sequenceStore, holeNum, holePos);

					std::vector<int> y(sequenceStore.size());

					y = sortera(sequenceStore, y);
					std::vector<int> vector1(n0 - gmerSize + 1, 1);
					std::vector<int> vector2(n1 - gmerSize + 1, 2);

					//std::vector<int> concVec = vector1;
					//concVec.insert(concVec.end(), vector2.begin(), vector2.end());
					std::vector<int> concVec = finalVecm0;
					std::vector<int> finalVec = reorder(concVec, y);


					//Function here that checks how many groups we have
					//Send sequences and finalVec
					Cstore[holeNum] = Cstore[holeNum] + calcC(sequenceStore, finalVec);
					//std::cout << Cstore[holeNum] << std::endl;
				}


			}

			std::vector <int > Nstore(M + 1);
			Nstore[0] = Cstore[0];

			for (int uuu = 1; uuu <= M; ++uuu)
			{
				int Cfactor = 0;
				for (int ttt = 0; ttt <= uuu; ++ttt)
				{
					Cfactor = Cfactor + nChoosek(gmerSize - ttt, M - ttt)*Nstore[ttt];

				}

				Nstore[uuu] = Cstore[uuu] - Cfactor;

			}

			int kernelValue = 0;

			for (int fff = 0; fff <= M; ++fff)
			{
				kernelValue = kernelValue + Nstore[fff] * nChoosek(gmerSize - fff, gmerSize - M);
			}

			kernelMatrix(zzz, ccc-(colSize-trainSize)) = kernelValue;
		}

	}

	return kernelMatrix;

}


//Allsequences contains all proteinsequences
//gmerSize is size of gmer
//M is number of holes
//trainsize is how many of the endstrings in Allsequences that belong to the testcases
//So store data as c("traincase1","traincase2",.."traincasen","testcase1","testcase2",..)
// [[Rcpp::export]]
NumericMatrix gakcokernel(std::vector< std::string > Allsequences, int gmerSize, int M, int trainSize)
{

	
	if (trainSize == 0)
	{
		return trainKernel(Allsequences, gmerSize, M);
	}
	else 
	{
		return predictKernel(Allsequences, gmerSize, M, trainSize);
	}


	
}