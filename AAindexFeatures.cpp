// Calculates the features from the AA-index database
//Import it into R with the rcpp package
//Call it as calcFeatures(sequences)
//where sequences is a character vector
//returns a matrix with all the features for all the sequences
//

#include <Rcpp.h>
#include <iostream>
#include <vector>
#include <algorithm>

// Enable C++11 via this plugin (Rcpp 0.10.3 or later)
// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

//Creates the gmers from the full strings.

// [[Rcpp::export]]
std::vector< double> totalComposition( std::string b)
{
	const int numAminoAcids = 20;
	char aminoAcids[numAminoAcids] = { 'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V' };

	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;

	for (int jjj = 0; jjj<stringSize; ++jjj)
	{
		for (int hhh = 0; hhh<numAminoAcids; ++hhh)
		{
			if (b[jjj] == aminoAcids[hhh])
			{
				featureVec[hhh] += 1;
				break;
			}

		}


	}
	for (int rrr = 0; rrr<numAminoAcids; ++rrr)
	{
		featureVec[rrr] = featureVec[rrr] / stringSize;
	}

	return featureVec;


}



// [[Rcpp::export]]
std::vector< double> createMuta( std::string  b) 
{
	const int numAcc = 20;
	char aminoAcids[numAcc] = {'A','R','N','D','C','Q','E','G','H','I','L','K','M','F','P','S','T','W','Y','V'};
	std::vector< double> accAA = { 0.80451070, -0.25405601 , 1.83283265 , 0.98597928, -1.61507035 , 0.59279736  ,0.86500023, -0.73797222 ,-0.22381125 , 0.68353165, -1.01017509, -0.52625888 , 0.62304212, -0.97993033, -0.52625888 , 1.40940596 , 0.71377641 ,-1.67555988 ,-0.97993033 , 0.01814686};
	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;
	std::vector< int> fairSplit = { ((stringSize / 10)),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10) };

	for (int iii = 0; iii < (stringSize - 10 * fairSplit[3]); ++iii) 
	{
		fairSplit[iii] += 1;
		
	}
	//This loop creates the featuresVectoe for the mean polarity
	int changeSplit = fairSplit[0];
	for (int kkk = 0; kkk < stringSize; ++kkk) 
	{
		
		
		if (kkk == changeSplit) 
		{



			featureIndex += 1;
			changeSplit = changeSplit + fairSplit[featureIndex];

			if (featureIndex == 10) 
			{
				featureIndex = 9;
			}
		}


		for (int ttt = 0; ttt < numAcc; ++ttt)
		{
			if (b[kkk] == aminoAcids[ttt])
			{
				if (accAA[ttt] > 0)
				{
					featureVec[2*featureIndex] += accAA[ttt];
				} else{
					featureVec[2*featureIndex+1] += accAA[ttt];
				}
					
			}
		}
	}

	for (int rrr=0;rrr<10;++rrr)
	{
		featureVec[2*rrr] = featureVec[2*rrr] / fairSplit[rrr]; //should divide by fairsplit maybe
		featureVec[2*rrr+1] = featureVec[2*rrr+1] / fairSplit[rrr];
	}


	return featureVec;


}





// [[Rcpp::export]]
std::vector< double> createAlpha( std::string  b) 
{
	const int numAcc = 20;
	char aminoAcids[numAcc] = {'A','R','N','D','C','Q','E','G','H','I','L','K','M','F','P','S','T','W','Y','V'};
	std::vector< double> accAA = { 1.20668730 , 0.46365325 ,-1.14130031, -0.30910217, -0.24965944 , 0.58253870 , 1.92000000, -1.67628483 ,-0.13077399,  0.04755418 , 0.99863777,0.73114551,  1.50390093 , 0.49337461, -1.58712074, -0.96297213 ,-0.72520124, -0.04160991 ,-0.99269350, -0.13077399 };
	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;
	std::vector< int> fairSplit = { ((stringSize / 10)),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10) };

	for (int iii = 0; iii < (stringSize - 10 * fairSplit[3]); ++iii) 
	{
		fairSplit[iii] += 1;
		
	}
	//This loop creates the featuresVectoe for the mean polarity
	int changeSplit = fairSplit[0];
	for (int kkk = 0; kkk < stringSize; ++kkk) 
	{
		
		
		if (kkk == changeSplit) 
		{



			featureIndex += 1;
			changeSplit = changeSplit + fairSplit[featureIndex];

			if (featureIndex == 10) 
			{
				featureIndex = 9;
			}
		}


		for (int ttt = 0; ttt < numAcc; ++ttt)
		{
			if (b[kkk] == aminoAcids[ttt])
			{
				if (accAA[ttt] > 0)
				{
					featureVec[2*featureIndex] += accAA[ttt];
				} else{
					featureVec[2*featureIndex+1] += accAA[ttt];
				}
					
			}
		}
	}

	for (int rrr=0;rrr<10;++rrr)
	{
		featureVec[2*rrr] = featureVec[2*rrr] / fairSplit[rrr]; //should divide by fairsplit maybe
		featureVec[2*rrr+1] = featureVec[2*rrr+1] / fairSplit[rrr];
	}


	return featureVec;


}





// [[Rcpp::export]]
std::vector< double> createVolume( std::string  b) 
{
	const int numAcc = 20;
	char aminoAcids[numAcc] = {'A','R','N','D','C','Q','E','G','H','I','L','K','M','F','P','S','T','W','Y','V'};
	std::vector< double> accAA = { -1.34649312 , 0.91163867 ,-0.45199097, -0.79958283, -0.76069144 , 0.12165715 ,-0.07766126 ,-2.03438472 , 0.21645494 , 0.60293820 , 0.60293820, 0.77551878 , 0.44980332,  1.10123424, -0.52248162, -1.33677027, -0.61971012 , 2.02490494 , 1.19846273, -0.05578485 };
	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;
	std::vector< int> fairSplit = { ((stringSize / 10)),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10) };

	for (int iii = 0; iii < (stringSize - 10 * fairSplit[3]); ++iii) 
	{
		fairSplit[iii] += 1;
		
	}
	//This loop creates the featuresVectoe for the mean polarity
	int changeSplit = fairSplit[0];
	for (int kkk = 0; kkk < stringSize; ++kkk) 
	{
		
		
		if (kkk == changeSplit) 
		{



			featureIndex += 1;
			changeSplit = changeSplit + fairSplit[featureIndex];

			if (featureIndex == 10) 
			{
				featureIndex = 9;
			}
		}


		for (int ttt = 0; ttt < numAcc; ++ttt)
		{
			if (b[kkk] == aminoAcids[ttt])
			{
				if (accAA[ttt] > 0)
				{
					featureVec[2*featureIndex] += accAA[ttt];
				} else{
					featureVec[2*featureIndex+1] += accAA[ttt];
				}
					
			}
		}
	}

	for (int rrr=0;rrr<10;++rrr)
	{
		featureVec[2*rrr] = featureVec[2*rrr] / fairSplit[rrr]; //should divide by fairsplit maybe
		featureVec[2*rrr+1] = featureVec[2*rrr+1] / fairSplit[rrr];
	}


	return featureVec;


}

// [[Rcpp::export]]
std::vector< double> createHydro( std::string  b) 
{
	const int numAcc = 20;
	char aminoAcids[numAcc] = {'A','R','N','D','C','Q','E','G','H','I','L','K','M','F','P','S','T','W','Y','V'};
	std::vector< double> accAA = { -0.46916215, -0.48126956, -1.13506972, -0.65077331 , 0.08777873, -1.20771418, -0.63866590, -1.12296231, -0.46916215 , 1.48013092 , 0.64471960, 0.18463801 , 0.22096024  ,1.23798271 , 1.15323084, -1.14717713 ,-1.14717713 , 2.00074956 , 1.06847896 , 0.39046398 };
	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;
	std::vector< int> fairSplit = { ((stringSize / 10)),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10) };

	for (int iii = 0; iii < (stringSize - 10 * fairSplit[3]); ++iii) 
	{
		fairSplit[iii] += 1;
		
	}
	//This loop creates the featuresVectoe for the mean polarity
	int changeSplit = fairSplit[0];
	for (int kkk = 0; kkk < stringSize; ++kkk) 
	{
		
		
		if (kkk == changeSplit) 
		{



			featureIndex += 1;
			changeSplit = changeSplit + fairSplit[featureIndex];

			if (featureIndex == 10) 
			{
				featureIndex = 9;
			}
		}


		for (int ttt = 0; ttt < numAcc; ++ttt)
		{
			if (b[kkk] == aminoAcids[ttt])
			{
				if (accAA[ttt] > 0)
				{
					featureVec[2*featureIndex] += accAA[ttt];
				} else{
					featureVec[2*featureIndex+1] += accAA[ttt];
				}
					
			}
		}
	}

	for (int rrr=0;rrr<10;++rrr)
	{
		featureVec[2*rrr] = featureVec[2*rrr] / fairSplit[rrr]; //should divide by fairsplit maybe
		featureVec[2*rrr+1] = featureVec[2*rrr+1] / fairSplit[rrr];
	}


	return featureVec;


}

// [[Rcpp::export]]
std::vector< double> createMeanPol( std::string  b) 
{
	const int numAcc = 20;
	char aminoAcids[numAcc] = {'A','R','N','D','C','Q','E','G','H','I','L','K','M','F','P','S','T','W','Y','V'};
	std::vector< double> accAA = { -0.2488, -1.1369, -0.727, -1.0914, 1.368, -1.0117, -1.05727, -0.64734, 0.37747, 1.3112, 1.19733, -1.52414, 1.2656, 1.26565, -0.18048, -0.749829, -0.48793, 0.8215, 0.19528, 1.0606929 };
	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;
	std::vector< int> fairSplit = { ((stringSize / 10)),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10) };

	for (int iii = 0; iii < (stringSize - 10 * fairSplit[3]); ++iii) 
	{
		fairSplit[iii] += 1;
		
	}
	//This loop creates the featuresVectoe for the mean polarity
	int changeSplit = fairSplit[0];
	for (int kkk = 0; kkk < stringSize; ++kkk) 
	{
		
		
		if (kkk == changeSplit) 
		{



			featureIndex += 1;
			changeSplit = changeSplit + fairSplit[featureIndex];

			if (featureIndex == 10) 
			{
				featureIndex = 9;
			}
		}


		for (int ttt = 0; ttt < numAcc; ++ttt)
		{
			if (b[kkk] == aminoAcids[ttt])
			{
				if (accAA[ttt] > 0)
				{
					featureVec[2*featureIndex] += accAA[ttt];
				} else{
					featureVec[2*featureIndex+1] += accAA[ttt];
				}
					
			}
		}
	}

	for (int rrr=0;rrr<10;++rrr)
	{
		featureVec[2*rrr] = featureVec[2*rrr] / fairSplit[rrr]; //should divide by fairsplit maybe
		featureVec[2*rrr+1] = featureVec[2*rrr+1] / fairSplit[rrr];
	}


	return featureVec;


}


// [[Rcpp::export]]
std::vector< double> createAcc( std::string  b) 
{
	const int numAcc = 20;
	char aminoAcids[numAcc] = {'A','R','N','D','C','Q','E','G','H','I','L','K','M','F','P','S','T','W','Y','V'};
	std::vector< double> accAA = { -0.076,-0.88802,-0.92579,-0.963564, 1.35918,-0.9163,-1.22794,-0.349829,0.245022,1.19867, 1.14201, -1.5584, 0.94376, 1.55747, -0.4159, -0.88802, -0.58588, 1.14201, 0.273348, 0.934294 };
	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;
	std::vector< int> fairSplit = { ((stringSize / 10)),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10),(stringSize / 10) };

	for (int iii = 0; iii < (stringSize - 10 * fairSplit[3]); ++iii) 
	{
		fairSplit[iii] += 1;
		
	}
	//This loop creates the featuresVectoe for the hydorphobicity
	int changeSplit = fairSplit[0];
	for (int kkk = 0; kkk < stringSize; ++kkk) 
	{
		
		
		if (kkk == changeSplit) 
		{



			featureIndex += 1;
			changeSplit = changeSplit + fairSplit[featureIndex];

			if (featureIndex == 10) 
			{
				featureIndex = 9;
			}
		}


		for (int ttt = 0; ttt < numAcc; ++ttt)
		{
			if (b[kkk] == aminoAcids[ttt])
			{
				if (accAA[ttt] > 0)
				{
					featureVec[2*featureIndex] += accAA[ttt];
				} else{
					featureVec[2*featureIndex+1] += accAA[ttt];
				}
					
			}
		}
	}

	for (int rrr=0;rrr<10;++rrr)
	{
		featureVec[2*rrr] = featureVec[2*rrr] / fairSplit[rrr]; //should divide by fairsplit maybe
		featureVec[2*rrr+1] = featureVec[2*rrr+1] / fairSplit[rrr];
	}


	return featureVec;


}




// [[Rcpp::export]]
NumericMatrix calcFeatures(std::vector< std::string > protSeq)
{

	int numStrings = protSeq.size();

	NumericMatrix featureMatrix(numStrings ,140);

	for (int rrr = 0; rrr < numStrings; ++rrr)
	{

		std::vector< double> accFeat = createAcc(protSeq[rrr]);
		std::vector< double> polarFeat = createMeanPol(protSeq[rrr]);
		std::vector< double> hydroFeat = createHydro(protSeq[rrr]);
		std::vector< double> volumeFeat = createVolume(protSeq[rrr]);
		std::vector< double> alphaFeat = createAlpha(protSeq[rrr]);
		std::vector< double> mutaFeat = createMuta(protSeq[rrr]);
		std::vector< double> totalFeat = totalComposition(protSeq[rrr]);
		
		for (int yyy=0;yyy<20;++yyy)
		{		
			featureMatrix(rrr,yyy) = accFeat[yyy];
		}
		for (int yyy=0;yyy<20;++yyy)
		{		
			featureMatrix(rrr,20+yyy) = polarFeat[yyy];
		}
		for (int yyy=0;yyy<20;++yyy)
		{		
			featureMatrix(rrr,20*2+yyy) = hydroFeat[yyy];
		}
		for (int yyy=0;yyy<20;++yyy)
		{		
			featureMatrix(rrr,20*3+yyy) = volumeFeat[yyy];
		}
		for (int yyy=0;yyy<20;++yyy)
		{		
			featureMatrix(rrr,20*4+yyy) = alphaFeat[yyy];
		}
		for (int yyy=0;yyy<20;++yyy)
		{		
			featureMatrix(rrr,20*5+yyy) = mutaFeat[yyy];
		}
		for (int yyy=0;yyy<20;++yyy)
		{		
			featureMatrix(rrr,20*6+yyy) = totalFeat[yyy];
		}
	}

    return featureMatrix;
}
