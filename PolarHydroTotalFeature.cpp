// Calculates the features for the polar hydrophobic and total composition of the sequences
// Import it into R with the rcpp package
// Call it as calcFeatures(sequences)
//Where sequences is a chartcer vector
//Returns a matrix with the features for the given sequences
//

#include <Rcpp.h>
#include <iostream>
#include <vector>
#include <algorithm>

// Enable C++11 via this plugin (Rcpp 0.10.3 or later)
// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

//Creates the gmers from the full strings.

// [[Rcpp::export]]
std::vector< double> totalComposition( std::string b)
{
	const int numAminoAcids = 20;
	char aminoAcids[numAminoAcids] = { 'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V' };

	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;

	for (int jjj = 0; jjj<stringSize; ++jjj)
	{
		for (int hhh = 0; hhh<numAminoAcids; ++hhh)
		{
			if (b[jjj] == aminoAcids[hhh])
			{
				featureVec[hhh] += 1;
				break;
			}

		}


	}
	for (int rrr = 0; rrr<numAminoAcids; ++rrr)
	{
		featureVec[rrr] = featureVec[rrr] / stringSize;
	}

	return featureVec;


}

// [[Rcpp::export]]
std::vector< double> createPolar( std::string  b)
{
	const int numPolar = 6;
	char polarAcids[numPolar] = {'R','K','E','D','Q','N'};

	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;
	std::vector< int> fairSplit = { ((stringSize / 4)),(stringSize / 4),(stringSize / 4),(stringSize / 4) };

	for (int iii = 0; iii < (stringSize - 4 * fairSplit[3]); ++iii)
	{
		fairSplit[iii] += 1;

	}
	//This loop creates the featuresVectoe for the hydorphobicity
	int changeSplit = fairSplit[0];
	for (int kkk = 0; kkk < stringSize; ++kkk)
	{


		if (kkk == changeSplit)
		{


			//std::cout << (stringSize) << std::endl;
			featureIndex += 1;
			changeSplit = changeSplit + fairSplit[featureIndex];

			if (featureIndex == 4)
			{
				featureIndex = 3;
			}
		}


		for (int ttt = 0; ttt < numPolar; ++ttt)
		{
			if (b[kkk] == polarAcids[ttt])
			{
				featureVec[featureIndex] += 1;
			}

		}
	}

	for (int rrr = 0; rrr<4; ++rrr)
	{
		featureVec[rrr] = featureVec[rrr] / stringSize;
	}


	return featureVec;


}




// [[Rcpp::export]]
std::vector< double> createHydrophobic( std::string  b) 
{
	const int numHydro = 7;
	char hydroAcids[numHydro] = {'C','V','L','I','M','F','W'};

	int stringSize = b.size();
	std::vector< double> featureVec = { 0,0,0,0 };
	int featureIndex = 0;
	int proportionIndex = 1;
	std::vector< int> fairSplit = { ((stringSize / 4)),(stringSize / 4),(stringSize / 4),(stringSize / 4) };

	for (int iii = 0; iii < (stringSize - 4 * fairSplit[3]); ++iii) 
	{
		fairSplit[iii] += 1;
		
	}
	//This loop creates the featuresVectoe for the hydorphobicity
	int changeSplit = fairSplit[0];
	for (int kkk = 0; kkk < stringSize; ++kkk) 
	{
		
		
		if (kkk == changeSplit) 
		{



			featureIndex += 1;
			changeSplit = changeSplit + fairSplit[featureIndex];

			if (featureIndex == 4) 
			{
				featureIndex = 3;
			}
		}


		for (int ttt = 0; ttt < numHydro; ++ttt)
		{
			if (b[kkk] == hydroAcids[ttt])
			{
				featureVec[featureIndex] += 1;
			}
		}
	}

	for (int rrr=0;rrr<4;++rrr)
	{
		featureVec[rrr] = featureVec[rrr] / stringSize;
	}


	return featureVec;


}




// [[Rcpp::export]]
NumericMatrix calcFeatures(std::vector< std::string > protSeq)
{

	int numStrings = protSeq.size();

	NumericMatrix featureMatrix(numStrings ,28);

	for (int rrr = 0; rrr < numStrings; ++rrr)
	{

		std::vector< double> hydroFeat = createHydrophobic(protSeq[rrr]);
		std::vector< double> polarFeat = createPolar(protSeq[rrr]);
		std::vector< double> totalFeat = totalComposition(protSeq[rrr]);
		
		for (int yyy=0;yyy<4;++yyy)
		{		
			featureMatrix(rrr,yyy) = hydroFeat[yyy];
		}
		for (int yyy=0;yyy<4;++yyy)
		{		
			featureMatrix(rrr,4+yyy) = polarFeat[yyy];
		}
		for (int yyy=0;yyy<20;++yyy)
		{		
			featureMatrix(rrr,4+4+yyy) = totalFeat[yyy];
		}
	}

    return featureMatrix;
}
