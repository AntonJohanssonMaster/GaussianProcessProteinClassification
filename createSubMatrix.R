
###Create submatrix from index vector and fullMatr
##indexVector is a vector specifying which components to keep
##fullMatr is the fullmatrix to create the sub matrix from

createSubMatrix = function(indexVec, fullMatr){

  newMatr = matrix(rep(0,length(indexVec)*length(indexVec)),nrow=length(indexVec))
  
  for (k in 1:length(indexVec))
  {
    for (j in k:length(indexVec))
    {
      newMatr[k,j] = fullMatr[indexVec[k],indexVec[j]]
      newMatr[j,k] = fullMatr[indexVec[k],indexVec[j]]
      
    }
    
  }
  return(newMatr)

}











