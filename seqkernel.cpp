// This is the c++ code for the Seqkernel
// Import it into R with the rcpp package 
// Call the function with seqkernel(sequences, gmersize, trainSize, subsmatrix)
// Where sequences should be a charater vector in R
//gmersize and trainsize should be numeric values and subsmatrix is the matrix which tells the relation between the amino acids
//In the thesis we use the BLOSUM62 matrix
//

#include <Rcpp.h>
#include <iostream>
#include <vector>
#include <algorithm>

// Enable C++11 via this plugin (Rcpp 0.10.3 or later)
// [[Rcpp::plugins(cpp11)]]

using namespace Rcpp;

//Creates the gmers from the full strings.


//function that turns each string into a sequence of numbers
// [[Rcpp::export]]
std::vector<int> turnSeqToNum(std::vector< std::string > &sequence)
{
	const int numOfAminoAcids = 20;
	char aminoacids[numOfAminoAcids] = { 'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V' };


	std::vector<int> numSeq(sequence[0].size() + sequence[1].size());
	for (int kkk = 0; kkk < 2; ++kkk)
	{
		for (int iii = 0 + kkk * sequence[0].size(); iii < (sequence[0].size() + sequence[1].size()*kkk); ++iii)
		{
			/*	switch (sequence[kkk][iii - kkk * sequence[0].size()])
			{
			case 'A':
			numSeq[iii] = 0;
			break;
			case 'B':
			numSeq[iii] = 1;
			break;
			//have to fix so all amino acids are here and so that the substition matrix reflects the internal order defined here in some way
			}
			*/

			for (int i = 0; i < numOfAminoAcids; ++i)
			{
				if (sequence[kkk][iii - kkk * sequence[0].size()] == aminoacids[i])
				{
					numSeq[iii] = i;
					break;
				}
			}
		}
	}

	return numSeq;

}

//create vector of gmers
// [[Rcpp::export]]
NumericMatrix createGmer(NumericMatrix &numMatr1, NumericMatrix &numMatr2, std::vector<int> numSequences, int gmerSize, int rowSize1, int rowSize2)
{
	for (int hhh = 0; hhh < rowSize1; ++hhh)
	{
		for (int jjj = 0; jjj < gmerSize; ++jjj)
		{
			numMatr1(hhh, jjj) = numSequences[jjj + hhh];
		}
	}
	for (int hhh = 0; hhh < rowSize2; ++hhh)
	{
		for (int jjj = 0; jjj < gmerSize; ++jjj)
		{
			numMatr2(hhh, jjj) = numSequences[jjj + hhh + rowSize1 + gmerSize - 1];
			//std::cout << jjj + hhh + rowSize1 + gmerSize - 1 << std::endl;
		}
	}
	return 0;
}


//Calc kernel value
// [[Rcpp::export]]
NumericMatrix calcKernelTrain(std::vector< std::string >  sequences, int gmerSize, NumericMatrix substitutionMatrix)
{
	//The parameter beta
	double beta = 0.2;
	std::vector<double> weightValues(gmerSize);


	std::vector< std::string > sequenceStore(2);
	NumericMatrix kernelMatr(sequences.size(), sequences.size());
	int sizeN1, sizeN2;
	//Loop through all sequences
	for (int ttt = 0; ttt < sequences.size(); ++ttt)
	{


		for (int rrr = ttt; rrr < sequences.size(); ++rrr)
		{

			std::vector<double> kernelV(gmerSize);

			for (int vvvv=0; vvvv<gmerSize; ++vvvv)
			{
				kernelV[vvvv] = 0;
			}

			//Load the given sequence
			sequenceStore[0] = sequences[rrr];
			sequenceStore[1] = sequences[ttt];

			sizeN1 = sequenceStore[0].size();
			sizeN2 = sequenceStore[1].size();

			//Store the weights for the kernels
			//The mean weight scheme is used
			for (int weightIndex = 0; weightIndex < gmerSize; ++weightIndex)
			{
				double meanWeight = ((sizeN1 - (weightIndex + 1) + 1)*(sizeN2 - (weightIndex + 1) + 1));
				weightValues[weightIndex] = 1 / meanWeight;
			}


			//Create indices from the letters in the sequence
			std::vector<int> numSequences = turnSeqToNum(sequenceStore);
			//Loop through all kmer sizes
			for (int kmerIndex = 1; kmerIndex <= gmerSize; ++kmerIndex)
			{

				//Stores the indices for each gmer for the two sequences
				NumericMatrix seq1(sizeN1 - kmerIndex + 1, kmerIndex);
				NumericMatrix seq2(sizeN2 - kmerIndex + 1, kmerIndex);
				//std::cout << 1 << std::endl;
				//The gmers are created here
				createGmer(seq1, seq2, numSequences, kmerIndex, sizeN1 - kmerIndex + 1, sizeN2 - kmerIndex + 1);
				//std::cout << 2 << std::endl;
				double kernelValue;

				//Loop through all gmers
				for (int qqq = 0; qqq < (sizeN1 - kmerIndex + 1); ++qqq)
				{

					for (int www = 0; www < (sizeN2 - kmerIndex + 1); ++www)
					{
						kernelValue = 1;

						for (int hhh = 0; hhh < kmerIndex; ++hhh)
						{

							kernelValue = kernelValue * pow(substitutionMatrix(seq1(qqq, hhh), seq2(www, hhh)), beta);


						}

						kernelV[kmerIndex-1] = kernelV[kmerIndex-1] + kernelValue;
					}
				}
			}

			//Weigh the kernel values now
			double kernelSum = 0;
			for (int kernelIndex = 0; kernelIndex<gmerSize; kernelIndex++)
			{
				kernelSum = kernelSum + weightValues[kernelIndex] * kernelV[kernelIndex];
			}


			//Store final value in the kernelMatrix
			kernelMatr(ttt, rrr) = kernelSum;
			kernelMatr(rrr, ttt) = kernelSum;
		}

	}
	return kernelMatr;
}

//Calc kernel value
// [[Rcpp::export]]
NumericMatrix calcKernelTest(std::vector< std::string >  sequences, int gmerSize, int trainSize, NumericMatrix substitutionMatrix)
{
	//The parameter beta
	double beta = 0.2;
	std::vector<double> weightValues(gmerSize);


	std::vector< std::string > sequenceStore(2);
	NumericMatrix kernelMatr(sequences.size(), trainSize);
	int sizeN1, sizeN2;
	//Loop through all sequences
	for (int ttt = 0; ttt < sequences.size(); ++ttt)
	{


		for (int rrr = 0; rrr < trainSize; ++rrr)
		{

			std::vector<double> kernelV(gmerSize);

			//Load the given sequence
			sequenceStore[0] = sequences[sequences.size() - trainSize + rrr];
			sequenceStore[1] = sequences[ttt];

			sizeN1 = sequenceStore[0].size();
			sizeN2 = sequenceStore[1].size();

			//Store the weights for the kernels
			//The mean weight scheme is used
			for (int weightIndex = 0; weightIndex < gmerSize; ++weightIndex)
			{
				double meanWeight = ((sizeN1 - (weightIndex + 1) + 1)*(sizeN2 - (weightIndex + 1) + 1));

				weightValues[weightIndex] = 1 / meanWeight;
			}


			//Create indices from the letters in the sequence
			std::vector<int> numSequences = turnSeqToNum(sequenceStore);
			//Loop through all kmer sizes
			for (int kmerIndex = 1; kmerIndex <= gmerSize; ++kmerIndex)
			{

				//Stores the indices for each gmer for the two sequences
				NumericMatrix seq1(sizeN1 - kmerIndex + 1, kmerIndex);
				NumericMatrix seq2(sizeN2 - kmerIndex + 1, kmerIndex);
				//std::cout << 1 << std::endl;
				//The gmers are created here
				createGmer(seq1, seq2, numSequences, kmerIndex, sizeN1 - kmerIndex + 1, sizeN2 - kmerIndex + 1);
				//std::cout << 2 << std::endl;
				double kernelValue;

				//Loop through all gmers
				for (int qqq = 0; qqq < (sizeN1 - kmerIndex + 1); ++qqq)
				{

					for (int www = 0; www < (sizeN2 - kmerIndex + 1); ++www)
					{
						kernelValue = 1;

						for (int hhh = 0; hhh < kmerIndex; ++hhh)
						{

							kernelValue = kernelValue * pow(substitutionMatrix(seq1(qqq, hhh), seq2(www, hhh)), beta);


						}

						kernelV[kmerIndex-1] = kernelV[kmerIndex - 1] + kernelValue;
					}
				}
			}

			//Weigh the kernel values now
			double kernelSum = 0;
			for (int kernelIndex = 0; kernelIndex<gmerSize; kernelIndex++)
			{
				kernelSum = kernelSum + weightValues[kernelIndex] * kernelV[kernelIndex];
			}


			//Store final value in the kernelMatrix
			kernelMatr(ttt, rrr) = kernelSum;
		}

	}
	return kernelMatr;
}

// [[Rcpp::export]]
NumericMatrix seqkernel(std::vector< std::string > sequences, int gmerSize, int trainSize, NumericMatrix substitutionMatrix)
{
	if (trainSize == 0)
	{
		return calcKernelTrain(sequences, gmerSize, substitutionMatrix);
	}
	else
	{
		return calcKernelTest(sequences, gmerSize, trainSize, substitutionMatrix);
	}
}