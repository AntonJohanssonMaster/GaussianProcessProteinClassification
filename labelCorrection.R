###Function that tries the outlier detection from "Active label Correction" paper
##posteriorGp should be the result returned from expectation propagation algorithm
## testData should be a list where the first componenent is the sequences or the features used for the sequences we want to investigate,..
##...second componenet shouild be the classes.

##numOfCorrections should be the amount of suggested corrections

labelCorrection = function(posteriorGP, testData, numOfCorrections = 10)
{
  

  predictions <- predictGP(posteriorGP, data.frame(testData[[1]]), labelCorr = "yes") #,labelCorr = "yes"
  proteinClasses = levels(factor(testData[[2]]))
  
  
  ##This loop checks the ALC-mislabeled
  predictedResult=data.frame(testData[[2]], predictions$labels) 
  
  propRight = 0
  for (i in 1:dim(predictedResult)[1]){
    if (predictedResult[i,1] == predictedResult[i,2]){
      propRight = propRight+1
    }
    
    
  }
  print("Correct Classification")
  print(propRight/dim(predictedResult)[1])
  
  
  ALCmislabel = c() #Contains the diffreence in probability
  ALCmislabelIndex = c() #Contains the corresponding index
  predictIndexStore = c()
  assignedIndexStore = c()
  for (i in 1:dim(predictedResult)[1]){
    if (predictedResult[i,1] != predictedResult[i,2]){
      
      assignedClassIndex = which(predictedResult[i,1] == proteinClasses)
      predictedClassIndex = which(predictedResult[i,2] == proteinClasses)
      ALCmislabel = c(ALCmislabel, predictions$prob[i,predictedClassIndex]-predictions$prob[i,assignedClassIndex])
      ALCmislabelIndex = c(ALCmislabelIndex, i)
      predictIndexStore = c(predictIndexStore, predictedClassIndex)
      assignedIndexStore = c(assignedIndexStore, assignedClassIndex)
    }
    
  }
  ##If they all were not correctly classified
  if (length(ALCmislabel) != 0)
  {
    sortedLabels = sort(ALCmislabel, decreasing =TRUE, index.return =TRUE)
    sortedIndex = ALCmislabelIndex[sortedLabels$ix]
    sortedPredict = predictIndexStore[sortedLabels$ix]
    sortedAssign = assignedIndexStore[sortedLabels$ix]
    correctMax = min(length(ALCmislabel),numOfCorrections)
    labelCorrString = c()
    for (k in 1:correctMax)
    {
      labelCorrString = c(labelCorrString, paste(proteinClasses[sortedAssign[k]],paste("-->",proteinClasses[sortedPredict[k]])))
      #cat(sortedIndex[k],"\t\t",proteinClasses[sortedAssign[k]],"->", proteinClasses[sortedPredict[k]],"\t\t", sortedLabels$x[k],"\t\t | \n")
    }
    
    
    printDataframe = data.frame(c(sortedIndex[1:correctMax]), labelCorrString, sortedLabels$x[1:correctMax])
    names(printDataframe) = c("Index", "Suggested Correction", "ALC-Value ( < 1 )")
    cat("\t\t\t---ALC-mislabeled---\n")
    print(printDataframe, print.gap = 10)
    
  } else
  {
    print("All observations were correctly classified!")  
  }
  
  
  ####ALC-disagreement here
  cat("\t\t\t---ALC-disagreement---\n")
  entropyALC = apply(-predictions$prob*log(predictions$prob),1,sum)
  sortedEntropy = sort(entropyALC, decreasing =TRUE, index.return =TRUE)
  
  entropyPrint = data.frame(sortedEntropy$ix[1:numOfCorrections],sortedEntropy$x[1:numOfCorrections])
  maxEntropy = sum(rep(-1/length(proteinClasses)*log(1/length(proteinClasses)),length(proteinClasses)))
  printStr = paste(c("ALC-value ( < ", format(maxEntropy,digits=4), " )"),collapse="")
  names(entropyPrint) = c("Index",printStr)
  print(entropyPrint, print.gap=10)
  
  indexRight = c()
  probRight = c()
  indexWrong = c()
  probWrong = c()
  for (i in 1:dim(predictedResult)[1]){
    if (predictedResult[i,1] == predictedResult[i,2]){
      indexRight = c(indexRight, i)
      probRight = c(probRight, predictions$maxprob[i])
    } else{
      indexWrong = c(indexWrong, i)
      probWrong = c(probWrong, predictions$maxprob[i])
    }
    
    
  }
  ggdf2 = data.frame(indexWrong, probWrong)
  library(ggplot2)
  ggplot(data=ggdf2, aes(x=ggdf2[,1],y=ggdf2[,2]))+geom_point()+#geom_point(data=ggdf2, aes(x=ggdf2[,1], y = ggdf2[,2]),shape = 18)+
    theme_minimal()+labs(y="MaximalClass Probability", x = "Index")+scale_x_continuous(limits = c(0, 1701))+scale_y_continuous(limits = c(0, 1))
  
  
}



